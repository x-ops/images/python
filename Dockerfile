ARG VERSION

# --- Base

FROM docker.io/library/python:${VERSION}-slim AS base

RUN set -eux \
  ; useradd \
    --create-home \
    --home-dir /home/app \
    --uid 1042 \
    --user-group \
    --shell /bin/bash \
    app \
  ; rm -f \
    /home/app/.bash_logout \
    ~/.bash_history \
    ~/.python_history \
    ~/.wget-hsts \
  ; rm -rf \
    /tmp/* \
    /var/tmp/* \
    ~/.cache \
    ~/.config

COPY --chown=root:root scripts/docker-entrypoint.sh /usr/bin/docker-entrypoint

RUN chmod 0755 /usr/bin/docker-entrypoint

WORKDIR /app

ENTRYPOINT ["docker-entrypoint"]

CMD ["bash"]

# --- Builder

FROM base AS builder

ENV PIP_NO_COLOR=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PIP_NO_INPUT=1
ENV PIP_PREFER_BINARY=1
ENV PIP_CACHE_DIR="/root/.cache/pip"
ENV PIPENV_CACHE_DIR="/root/.cache/pipenv"
ENV POETRY_CACHE_DIR="/root/.cache/poetry"
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN set -eux \
  ; apt-get update -qq \
  ; apt-get install -yq --no-install-recommends \
    build-essential \
    sudo \
  ; apt-get -yq clean \
  ; echo 'app ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/app \
  ; chown root:root /etc/sudoers.d/app \
  ; chmod 0644 /etc/sudoers.d/app \
  ; su -c 'sudo whoami' app \
  ; mkdir -p "$PIP_CACHE_DIR" "$PIPENV_CACHE_DIR" "$POETRY_CACHE_DIR" \
  ; pip install --upgrade \
    pip \
    pipenv \
    poetry \
  ; pip --version \
  ; pipenv --version \
  ; poetry --version \
  ; rm -f \
    ~/.bash_history \
    ~/.python_history \
    ~/.wget-hsts \
  ; rm -rf \
    /tmp/* \
    /var/tmp/* \
    /var/lib/apt/lists/* \
    "$PIP_CACHE_DIR"/* \
    "$PIPENV_CACHE_DIR"/* \
    "$POETRY_CACHE_DIR"/* \
  ; find /usr/local -depth \
    '(' \
      '(' -type d -a \
        '(' -name test -o -name tests ')' \
      ')' \
      -o \
      '(' -type f -a \
        '(' -name '*.pyc' -o -name '*.pyo' ')' \
      ')' \
    ')' \
    -exec rm -rf '{}' +;
