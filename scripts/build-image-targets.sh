#!/usr/bin/env bash

set -eu

getImage() {
    local registry repository target version
    target=${1}
    version=${VERSION}

    if [ -n "${CI:-}" ] && [ -n "${CI_REGISTRY_IMAGE:-}" ]; then
        registry=$(echo "$CI_REGISTRY_IMAGE" | cut -d '/' -f 1)
        repository=$(echo "$CI_REGISTRY_IMAGE" | cut -d '/' -f 2-)
    else
        registry=${IMAGE_REGISTRY:-"registry.gitlab.com"}
        repository=${IMAGE_REPOSITORY:-"x-ops/images/python"}
    fi

    echo "$registry/$repository/$target:$version"
}

if [ $# -eq 0 ]; then
    echo "Usage: $(basename "$0") <Python version>" >&2
    exit 1
else
    export VERSION="$1"
    shift
fi

base=$(getImage base)
builder=$(getImage builder)

docker build . --build-arg VERSION --target base --tag "$base" --no-cache "$@"
docker push "$base"
docker build . --build-arg VERSION --target builder --tag "$builder" --cache-from "$base" "$@"
docker push "$builder"
